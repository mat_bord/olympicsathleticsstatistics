import asyncio
from bs4 import BeautifulSoup
from pyppeteer import launch
import os
import csv
import pandas as pd

nationality, name, result, pbsb =([] for i in range(5))
async def main():
    # Launch the browser
    browser = await launch()
    # Open a new browser page
    page = await browser.newPage()
    prelink="https://olympics.com/tokyo-2020/olympic-games/en/results/athletics/result-men-s-100m-"
    list_pagepath=["prel-000100-",
                "prel-000200-",
                "prel-000300-",
                "rnd1-000100-",
                "rnd1-000200-",
                "rnd1-000300-",
                "rnd1-000400-",
                "rnd1-000500-",
                "rnd1-000600-",
                "rnd1-000700-",
                "sfnl-000100-",
                "sfnl-000200-",
                "sfnl-000300-",
                "fnl-000100-"]
    for single_pagepath in list_pagepath:
        # Create a URI for our test file
        page_path = prelink+single_pagepath+".htm"

        # Open our test file in the opened page
        await page.goto(page_path)
        page_content = await page.content()

        # Process extracted content with BeautifulSoup
        soup = BeautifulSoup(page_content, "html.parser")

        firsthalf=single_pagepath.split("-")[0].upper()
        secondhalf=single_pagepath.split("-")[1]
        detail=(firsthalf+"-"+secondhalf if firsthalf=="FNL" else firsthalf+secondhalf)
        print(detail)
        results = soup.find(id="ATHATHR073AATHM100M--------------"+detail+"--_wrapper")
        job_elements = results.find_all("tr", {'class':['odd', 'even']})
        for job_element in job_elements:
            nationality.append(job_element.find("abbr", class_="noc").text.strip('\n'))
            name.append(job_element.find("span", class_="d-none d-md-inline").text.strip('\n'))
            info=job_element.findAll("td", class_="text-center")[5].text.strip('\n')
            if (info=='DNF' or info=='DQ' or info=='DNS'):
                result.append(100.0)
            else:
                result.append(float(info))
            info=job_element.find("td", class_="text-right")
            if (info==None):
                pbsb.append(" ")
            else:
                pbsb.append(info.text.strip('\n').strip('='))

        # Close browser
    await browser.close()


asyncio.get_event_loop().run_until_complete(main())
data = pd.DataFrame(list(zip(nationality, name, result, pbsb)),
               columns =['Nationality', 'Name', 'Result', 'pbsb'])

#order by result and keep only the best result from each athlete: 
data=data.sort_values(by=['Result'])

data=data.drop_duplicates(subset=['Name'], keep='first', ignore_index=True)
#Now I have only one result for each athlete: n lines==n athletes
#Save to csv
data.to_csv('dataset_olympics_men_100m.csv', encoding='utf-8', index=False)



