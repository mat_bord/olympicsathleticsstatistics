import asyncio
from bs4 import BeautifulSoup
from pyppeteer import launch
import os
import csv
import pandas as pd

datafillin={'Place':['Tokyo-2020'], 'Result':[22.25]}
data = pd.DataFrame(datafillin)

async def main():
    global data
    # Launch the browser
    browser = await launch()
    # Open a new browser page
    page = await browser.newPage()
    prelink="https://olympics.com/en/olympic-games/"
    list_pagepath=["rio-2016", "london-2012", "beijing-2008", "athens-2004", "sydney-2000", "atlanta-1996", "barcelona-1992",
                    "seoul-1988","los-angeles-1984", "moscow-1980", "montreal-1976", "munich-1972", "mexico-city-1968", "tokyo-1964",
                    "rome-1960","melbourne-1956","helsinki-1952","london-1948"]

    for single_pagepath in list_pagepath:
        print(single_pagepath)
        nationality, name, result=([] for i in range(3))
        # Create a URI for our test file
        page_path = prelink +single_pagepath+"/results/athletics/200m-women"

        # Open our test file in the opened page
        await page.goto(page_path)
        page_content = await page.content()

        # Process extracted content with BeautifulSoup
        soup = BeautifulSoup(page_content, "html.parser")
        results = soup.find("div", class_="styles__ResultTableWrapper-sc-1i210af-1")
        job_elements = results.find_all("div", {'class':['styles__Row-sc-rh9yz9-11 styles__SingleAthleteResultRowGrid-sc-e5c43x-0 cgtEht IJoCN']})
        for job_element in job_elements:
            nationality.append(job_element.find("span", class_="styles__CountryName-sc-1r5phm6-1").text.strip('\n'))
            name.append(job_element.find("h3", class_="styles__AthleteName-sc-1yhe77y-3").text.strip('\n'))
            info=job_element.find("span", class_="styles__Info-sc-cjoz4h-0").text.strip('\n')
            if (info=='Results:'):
                result.append(100.0)
            else:
                time=info.split(":")[1]
                time=time.split("w")[0]
                time=time.split("est")[0]
                result.append(float(time))
        
        singledata=pd.DataFrame(list(zip(nationality, name, result)),
                columns =['Nationality', 'Name', 'Result'])
        
        singledata = singledata[singledata.Result != 100]
        mean=singledata["Result"].mean()
        singleresult={'Place':single_pagepath, 'Result':mean}
        data=data.append(singleresult, ignore_index=True)

        # Close browser
    await browser.close()


asyncio.get_event_loop().run_until_complete(main())

print(data)

# #Save to csv
data.to_csv('history_olympics_200m_mean_women.csv', encoding='utf-8', index=False)



